package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type User struct {
	Name            string `json:"name"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
}

func main() {
	http.HandleFunc("/register", registerHandler)
	fmt.Println("Server listening on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Método não permitido", http.StatusMethodNotAllowed)
		return
	}

	var newUser User
	err := json.NewDecoder(r.Body).Decode(&newUser)
	if err != nil {
		http.Error(w, "Erro ao decodificar o JSON", http.StatusBadRequest)
		return
	}

	// Verificar se as senhas coincidem
	if newUser.Password != newUser.ConfirmPassword {
		http.Error(w, "As senhas não coincidem", http.StatusBadRequest)
		return
	}

	// Aqui você pode adicionar a lógica de salvar o usuário no banco de dados ou realizar outras operações necessárias
	fmt.Printf("Novo usuário registrado: %+v\n", newUser)

	// Responder ao cliente
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Usuário registrado com sucesso!")
}

